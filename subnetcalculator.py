def ask_for_number_sequence(message):
    print(message)
    ip_adress = input()
    return [int(elem) for elem in ip_adress.split(".")]



def is_valid_ip_address(numberlist):
    count = 0
    for n in numberlist:
      if n < 0 or n > 255:
        count = (count +1)
    if len(numberlist) != 4:
        count = (count +1)       
    if count > 0:
     return False
    else:
         return True


def is_valid_netmask(numberlist):
    if len(numberlist) != 4:
        return False
    binary_netmask = ""
    for getal in numberlist:
        binary_netmask  = binary_netmask  + f"{getal:08b}"
    checking_ones = True
    for getal in binary_netmask:
        if getal == "1" and checking_ones == True:
            checking_ones = True
        if getal == "0" and checking_ones == True:
            checking_ones = False
        if getal == "0" and checking_ones ==False:
            checking_ones = False
        if getal == "1" and checking_ones ==False:
            return False
            break
    return True


def one_bits_in_netmask(numberlist):
    binary_netmask = ""
    for getal in numberlist:
        binary_netmask  = binary_netmask  + f"{getal:08b}"
        counter =0
    for getal in binary_netmask:
        if getal == "1":
            counter +=1
    return counter



def apply_network_mask(host_address ,netmask ):
        deel1= host_address[0] & netmask[0] 
        deel2= host_address[1] & netmask[1]    
        deel3= host_address[2] & netmask[2]
        deel4= host_address[3] & netmask[3]
        return str(deel1)+"."+ str(deel2)+"."+str(deel3)+"."+str(deel4)





def netmask_to_wilcard_mask(netmask):
        wildcard_mask = []
        wildcard_deel1=""
        wildcard_deel2=""
        wildcard_deel3=""
        wildcard_deel4=""
        for bit in f"{netmask[0]:08b}":
            if bit == "0":
                wildcard_deel1 += "1"
            else:
                wildcard_deel1 += "0"
        for bit in f"{netmask[1]:08b}":
             if bit == "0":
                wildcard_deel2 += "1"
             else:
                wildcard_deel2 += "0"

        for bit in f"{netmask[2]:08b}":
             if bit == "0":
                 wildcard_deel3 += "1"
             else:
                 wildcard_deel3 += "0" 
        for bit in f"{netmask[3]:08b}":
             if bit == "0":
                 wildcard_deel4 += "1"
             else:
                 wildcard_deel4 += "0"
        wildcard_mask = str(int(wildcard_deel1,2))+"."+str(int(wildcard_deel2,2))+"."+ str(int(wildcard_deel3,2))+"."+ str(int(wildcard_deel4,2))
        return [int(elem) for elem in wildcard_mask.split(".")]






def get_broadcast_address(network_address, wildcard_mask):
        deel1= network_address[0] | wildcard_mask[0] 
        deel2= network_address[1] | wildcard_mask[1]    
        deel3= network_address[2] | wildcard_mask[2]
        deel4= network_address[3] | wildcard_mask[3]
        broadcast = str(deel1)+"."+ str(deel2)+"."+str(deel3)+"."+str(deel4)       
        return [int(elem) for elem in broadcast.split(".")]



def prefix_length_to_max_hosts(getal):
   uitkomst = pow(getal,2)-2
   return uitkomst
        




ip = ask_for_number_sequence("Wat is het IP-adres?")
mask = ask_for_number_sequence("Wat is het Subnetmasker?")

iptrue = is_valid_ip_address(ip)
 
masktrue = is_valid_netmask(mask)

if masktrue ==True and iptrue == True:
    print('IP-adres en subnetmasker zijn geldig.')

else:
    print('IP-adres en/of subnetmasker is ongeldig.')
    exit()


one = one_bits_in_netmask(mask)
print("De lengte van het subnetmasker is " + str(one_bits_in_netmask(mask)))

print("Het adres van het subnet is " + str(apply_network_mask(ip,mask)))

wild = netmask_to_wilcard_mask(mask)
print("Het wildcardmasker is " + str(netmask_to_wilcard_mask(mask)))
print("Het broadcastadres is " + str(get_broadcast_address(ip,wild)))


print("Het maximaal aantal hosts op dit subnet is " + str(prefix_length_to_max_hosts(one)))
        

     
          






